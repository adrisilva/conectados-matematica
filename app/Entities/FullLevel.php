<?php

namespace App\Entities;

use App\Scopes\SchoolScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class FullLevel.
 *
 * @package namespace App\Entities;
 */
class FullLevel extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SchoolScope);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'theme_id', 'points_acquired', 'school_id'];

}
