<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Question;
use App\Entities\Theme;
use App\Http\Requests\CreateContentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller
{
    public function index()
    {
        $themes = Theme::all();
        return view('conteudo.index', compact('themes'));
    }

    public function create()
    {
        return view('conteudo.create');
    }

    public function store(CreateContentRequest $request, $school)
    {
        DB::beginTransaction();

        $url_class = str_replace('watch?v=', 'embed/', $request->link_class);

        $request->merge([
            'link_class' => $url_class
        ]);

        $theme = Theme::create($request->all());
        foreach($request->questions as $question){
            $theme->questions()->create([
                'question' => $question['question'],
                'alternative_a' => $question['alternative_a'],
                'alternative_b' => $question['alternative_b'],
                'alternative_c' => $question['alternative_c'],
                'alternative_d' => $question['alternative_d'],
                'alternative_e' => $question['alternative_e'],
                'resolution' => $question['resolution'],
                'right_alternate' => $question['right_alternate'],
                'score' => 50
            ]);
        }
        DB::commit();

        return redirect('admin/conteudos')->withSuccess('Conteúdo cadastrado com sucesso');
    }

    public function edit($school, $theme_id)
    {
        $theme = Theme::find($theme_id);
        return view('conteudo.edit', compact('theme'));
    }

    public function update(CreateContentRequest $request, $school, $theme_id)
    {
        DB::beginTransaction();

        $url_class = str_replace('watch?v=', 'embed/', $request->link_class);

        $request->merge([
            'link_class' => $url_class
        ]);

        $theme = Theme::find($theme_id)
                    ->update($request->all());

        foreach($request->questions as $question){
            Question::find($question['id'])->update([
                'question' => $question['question'],
                'alternative_a' => $question['alternative_a'],
                'alternative_b' => $question['alternative_b'],
                'alternative_c' => $question['alternative_c'],
                'alternative_d' => $question['alternative_d'],
                'alternative_e' => $question['alternative_e'],
                'resolution' => $question['resolution'],
                'right_alternate' => $question['right_alternate'],
                'score' => 50
            ]);
        }
        DB::commit();

        return redirect('admin/conteudos')->withSuccess('Dados atualizados com sucesso');
    }

    public function delete($school, $theme_id)
    {
        $theme = Theme::find($theme_id);
        $theme->questions()->delete();
        $theme->delete();

        return redirect('admin/conteudos')->withSuccess('Dados deletados com sucesso');
    }
}
