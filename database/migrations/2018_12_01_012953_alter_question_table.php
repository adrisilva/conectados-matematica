<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('questions', 'question')){
            Schema::table('questions', function(Blueprint $table){
                $table->dropColumn('question');
                $table->dropColumn('alternative_a');
                $table->dropColumn('alternative_b');
                $table->dropColumn('alternative_c');
                $table->dropColumn('alternative_d');
                $table->dropColumn('alternative_e');
                $table->dropColumn('resolution');
            });
        }

        Schema::table('questions', function(Blueprint $table){
            $table->longText('question');
            $table->longText('alternative_a');
            $table->longText('alternative_b');
            $table->longText('alternative_c');
            $table->longText('alternative_d');
            $table->longText('alternative_e');
            $table->longText('resolution');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function(Blueprint $table){
            $table->dropColumn('question');
            $table->dropColumn('alternative_a');
            $table->dropColumn('alternative_b');
            $table->dropColumn('alternative_c');
            $table->dropColumn('alternative_d');
            $table->dropColumn('alternative_e');
            $table->dropColumn('resolution');
        });

        Schema::table('questions', function(Blueprint $table) {
            $table->text('question');
            $table->text('alternative_a');
            $table->text('alternative_b');
            $table->text('alternative_c');
            $table->text('alternative_d');
            $table->text('alternative_e');
        });
    }
}
