<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppMenuProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $event->menu->add('MENU PRINCIPAL');
            
            if(auth()->guard('admin')->check()){

                $event->menu->add([
                    'text' => 'Dashboard',
                    'url'  => '/admin/dashboard',
                    'icon' => 'dashboard'
                ]);

//                $event->menu->add([
//                    'text' => 'Alunos',
//                    'url'  => '/admin/alunos',
//                    'icon' => 'user'
//                ]);
//
//                $event->menu->add([
//                    'text' => 'Professores',
//                    'url'  => '/admin/professores',
//                    'icon' => 'graduation-cap'
//                ]);

                $event->menu->add([
                    'text' => 'Conteúdo',
                    'url'  => '/admin/conteudos',
                    'icon' => 'pencil'
                ]);
                
            }else{

                $event->menu->add([
                    'text' => 'Home',
                    'url'  => '/home',
                    'icon' => 'home'
                ]);

//                $event->menu->add([
//                    'text' => 'Vídeo-Aulas',
//                    'url'  => '/aulas',
//                    'icon' => 'youtube-play'
//                ]);

                $event->menu->add([
                    'text' => 'Ranking',
                    'url'  => '/ranking',
                    'icon' => 'trophy'
                ]);
//
//                $event->menu->add([
//                    'text' => 'Seu Perfil',
//                    'url'  => '/perfil',
//                    'icon' => 'user'
//                ]);

                
            }
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
