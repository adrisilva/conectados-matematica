<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('theme_id');
            $table->unsignedInteger('school_id');
            $table->string('question');
            $table->string('alternative_a');
            $table->string('alternative_b');
            $table->string('alternative_c');
            $table->string('alternative_d');
            $table->string('alternative_e');
            $table->enum('right_alternate', ['a', 'b', 'c', 'd', 'e']);
            $table->string('resolution');
            $table->integer('score');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('theme_id')
                ->references('id')
                ->on('themes')
                ->onDelete('cascade');

            $table->foreign('school_id')
                ->references('id')
                ->on('schools')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
