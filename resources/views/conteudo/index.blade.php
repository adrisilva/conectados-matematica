@extends('adminlte::page')

@section('content_header')
    <h1>Conteúdos</h1>

    <br>
    <a class="btn btn-social btn-success" href="{!! url('admin/conteudos/novo') !!}">
        <i class="fa fa-plus"></i> Novo Conteúdo
    </a>

    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Conteúdos</a></li>
    </ol>
@stop

@section('content')
    @component('includes.modals')
        Tem certeza que deseja excluir esse tema juntamente com suas questões?
    @endcomponent

    <div class="box">
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr role="row">
                        <th width="350">Título</th>
                        <th width="50">Editar</th>
                        <th width="50">Excluir</th>
                    </tr>
                    </thead>

                    <tbody>

                    @forelse ($themes as $theme)
                        <tr>
                            <th>{!! $theme->title !!}</th>
                            <th>
                                <a class="btn btn-primary" href="{!! url('admin/conteudos/'.$theme->id.'/edit') !!}">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </th>
                            <th>
                                <a class="btn btn-danger btn-delete" data-toggle="modal" data-target="#modal-danger" data-id="{!! $theme->id !!}">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </th>
                        </tr>
                    @empty
                        <tr>
                            <th colspan="7">Nenhum Conteúdo Encontrado</th>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@section('js')
    <script>
        $('.btn-delete').on('click', function () {
            var theme_id = $(this).data('id');
            var url = '{!! url('admin/conteudos/{id}/delete') !!}'.replace('{id}', theme_id);
            $('#form-delete').attr('action', url);
        });
    </script>
@endsection
@stop
