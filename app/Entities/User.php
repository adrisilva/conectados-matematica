<?php

namespace App\Entities;

use App\Scopes\SchoolScope;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User.
 *
 * @package namespace App\Entities;
 */
class User extends Authenticatable implements Transformable
{
	use Notifiable;
    use TransformableTrait;
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SchoolScope);
    }
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'registration', 'birth', 'course', 'series',
     'shift', 'type', 'matter', 'email', 'password', 'school_id', 'first_access', 'experience'];

    public function score()
    {
    	return $this->hasMany('App\Entities\FullLevel');
    }
}
