<?php

namespace App\Observers;

use App\Entities\School;
use App\Entities\Theme;

class ThemeObserver
{
    private $school;

    public function __construct()
    {
        $this->school = School::where('slug', request()->school)->first();
    }
    /**
     * Handle the theme "created" event.
     *
     * @param  \App\Entities\Theme  $theme
     * @return void
     */
    public function created(Theme $theme)
    {
        //
    }

    /**
     * Handle the theme "updated" event.
     *
     * @param  \App\Entities\Theme  $theme
     * @return void
     */
    public function updated(Theme $theme)
    {
        //
    }

    /**
     * Handle the theme "deleted" event.
     *
     * @param  \App\Entities\Theme  $theme
     * @return void
     */
    public function deleted(Theme $theme)
    {
        //
    }

    /**
     * Handle the theme "restored" event.
     *
     * @param  \App\Entities\Theme  $theme
     * @return void
     */
    public function restored(Theme $theme)
    {
        //
    }

    /**
     * Handle the theme "force deleted" event.
     *
     * @param  \App\Entities\Theme  $theme
     * @return void
     */
    public function forceDeleted(Theme $theme)
    {
        //
    }

    public function creating(Theme $theme)
    {
        $theme->school_id = $this->school->id;
    }
}
