<?php

use Illuminate\Database\Seeder;
use App\Entities\Theme;

class ThemesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Theme::create([
            'school_id'     => '1',
        	'title'			=> 'Operações Básicas',
        	'description'	=> 'Quatro Operações Básicas: Soma, Subtração, Multiplicação e Divisão.',
        	'link_class'	=> 'https://www.youtube.com/embed/XlyGk8HZUPQ'
        ]);

        Theme::create([
            'school_id'     => '1',
        	'title'			=> 'Equação do 1º Grau',
        	'description'	=> 'Funções do primeiro grau básico.',
        	'link_class'	=> 'https://www.youtube.com/embed/QthgjnKjWCo'
        ]);

        Theme::create([
            'school_id'     => '1',
        	'title'			=> 'Regra de Três Simples',
        	'description'	=> 'Regra de três composta.',
        	'link_class'	=> 'https://www.youtube.com/embed/sJIuwfpyyM4'
        ]);

        Theme::create([
            'school_id'     => '1',
        	'title'			=> 'Regra de Três Composta',
        	'description'	=> 'Regra de três composta.',
        	'link_class'	=> 'https://www.youtube.com/embed/K2r1CtLDeVE'
        ]);

    }
}
