<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateFullLevelsTable.
 */
class CreateFullLevelsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('full_levels', function(Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('school_id');
            $table->unsignedInteger('user_id');
			$table->unsignedInteger('theme_id');
            $table->double('points_acquired', 2, 1);
            $table->softDeletes();
            $table->timestamps();

		    $table->foreign('user_id')
		    		->references('id')
		    		->on('users')
		    		->onDelete('cascade');

            $table->foreign('theme_id')
		            ->references('id')
		            ->on('themes')
					->onDelete('cascade');
					
			$table->foreign('school_id')
                ->references('id')
                ->on('schools')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('full_levels');
	}
}
