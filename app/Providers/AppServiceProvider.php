<?php

namespace App\Providers;

use App\Entities\Question;
use App\Entities\SelectedAlternative;
use App\Entities\Theme;
use App\Observers\QuestionObserver;
use App\Observers\SelectedQuestionObserver;
use App\Observers\ThemeObserver;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        Schema::defaultStringLength(191);
        Theme::observe(ThemeObserver::class);
        Question::observe(QuestionObserver::class);
        SelectedAlternative::observe(SelectedQuestionObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
