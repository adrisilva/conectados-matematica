<div id="preloader">
  <div id="status"></div>
</div>

<div id="container">
</div>

<style>
  #preloader{position:fixed;top:0; left:0;right:0;bottom:0;background:#ECF0F1;z-index:9999;height:100%;width:100%;}
  #status{width:920px;height:532px;position:absolute;left:50%;top:50%;margin:-266px 0 0 -460px;background:url({{asset('img/preloader.gif')}}) center no-repeat;}
</style>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script>
  let url = '{!! url('load-themes') !!}';

  $.get(url, function(response){
    $.each(response, function(key, value){

      var title       = value.title;
      var description = value.description;
      var injectScore = '';
      var url         = 'aula/tema/'+value.id;

      for(let i = 0; i < value.scores.length; i++){
        var score = value.scores[i].points_acquired;
      }

      if(typeof score != "undefined"){
        injectScore = 'Sua pontuação: ' + score + '/5';
      }else{
        injectScore = 'Sua pontuação: 0/5';
      }

      $('#container').append(
        '<div class="box box-primary"><div class="box-header ui-sortable-handle"><h3 class="box-title">'
        + title + '</h3></div><div class="box-body">'+ description +'</div> <div class="box-footer"><div class="input-group"><strong>'+ injectScore +'</strong>&nbsp;<a href='+  url +' class="btn btn-primary"><i class="fa fa-play-circle"></i>&nbsp;Jogar</a></div></div> </div>');
    });

    $('#preloader').delay(1000).fadeOut("slow");

  });
</script>