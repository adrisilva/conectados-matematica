<?php

namespace App\Entities;

use App\Scopes\SchoolScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Question.
 *
 * @package namespace App\Entities;
 */
class Question extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SchoolScope);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['theme_id', 'question', 'alternative_a', 'alternative_b', 'alternative_c', 'alternative_d'
    , 'alternative_e', 'right_alternate', 'resolution', 'score', 'school_id'];

}
