<?php

namespace App\Observers;

use App\Entities\School;
use App\Entities\SelectedAlternative;

class SelectedQuestionObserver
{
    private $school;

    public function __construct()
    {
        $this->school = School::where('slug', request()->school)->first();
    }
    /**
     * Handle the selected alternative "created" event.
     *
     * @param  \App\Entities\SelectedAlternative  $selectedAlternative
     * @return void
     */
    public function created(SelectedAlternative $selectedAlternative)
    {
        //
    }

    /**
     * Handle the selected alternative "updated" event.
     *
     * @param  \App\Entities\SelectedAlternative  $selectedAlternative
     * @return void
     */
    public function updated(SelectedAlternative $selectedAlternative)
    {
        //
    }

    /**
     * Handle the selected alternative "deleted" event.
     *
     * @param  \App\Entities\SelectedAlternative  $selectedAlternative
     * @return void
     */
    public function deleted(SelectedAlternative $selectedAlternative)
    {
        //
    }

    /**
     * Handle the selected alternative "restored" event.
     *
     * @param  \App\Entities\SelectedAlternative  $selectedAlternative
     * @return void
     */
    public function restored(SelectedAlternative $selectedAlternative)
    {
        //
    }

    /**
     * Handle the selected alternative "force deleted" event.
     *
     * @param  \App\Entities\SelectedAlternative  $selectedAlternative
     * @return void
     */
    public function forceDeleted(SelectedAlternative $selectedAlternative)
    {
        //
    }

    public function creating(SelectedAlternative $selectedAlternative)
    {
        $selectedAlternative->school_id = $this->school->id;
    }
}
