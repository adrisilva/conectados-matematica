<?php

namespace App\Http\Requests;

use App\Entities\Theme;
use Illuminate\Foundation\Http\FormRequest;

class CreateContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Theme::$rules;
    }

    public function messages()
    {
        return [
            'title.required'         => 'Títuo obrigatório',
            'description.required'   => 'Descrição obrigatório',
            'link_class.required'    => 'Link da aula obrigatório',
            'questions.*.question.required'         => 'Todos os campos são obrigatórios',
            'questions.*.alternative_a.required'    => 'Todos os campos são obrigatórios',
            'questions.*.alternative_b.required'    => 'Todos os campos são obrigatórios',
            'questions.*.alternative_c.required'    => 'Todos os campos são obrigatórios',
            'questions.*.alternative_d.required'    => 'Todos os campos são obrigatórios',
            'questions.*.alternative_e.required'    => 'Todos os campos são obrigatórios',
            'questions.*.resolution.required'       => 'Todos os campos são obrigatórios',
            'questions.*.right_alternate.required'  => 'Todos os campos são obrigatórios',
        ];
    }
}
