<?php

namespace App\Http\Controllers;

use App\Entities\FullLevel;
use App\Entities\School;
use App\Entities\Theme;
use App\Entities\Question;
use Illuminate\Http\Request;
use App\Entities\SelectedAlternative;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class QuizController extends Controller
{
    public function start($school, $theme_id)
    {
        $theme = Theme::with('questions')->find($theme_id);
        $questions_id = [];

        foreach($theme->questions as $question){
            array_push($questions_id, $question->id);
        }

        Config::set('adminlte.collapse_sidebar', true);
        return view('plataforma.quiz', compact('questions_id', 'theme'));
    }

    public function loadQuestion($school, $questions_id)
    {
        $question = Question::select('id', 'question', 'alternative_a', 'alternative_b',
         'alternative_c', 'alternative_d', 'alternative_e')
                ->where('id', $questions_id)
                ->get();

        return response()->json($question); 
    }

    public function verifyQuestion(Request $request)
    {
        $school = School::where('slug', $request->school)->first();

        $request->merge([
            'user_id'   => $request->user()->id,
            'school_id' => $school->id
        ]);

        SelectedAlternative::create($request->all());

        $question = Question::select('question', 'resolution')
                ->where('id', $request->question_id)
                ->get();

        return response()->json($question);
    }

    public function calculatePoints(Request $request)
    {
        $school = School::where('slug', request()->school)->first();

        $gabarito = [];
        $correct = 0;

        $theme = Theme::find(request()->theme_id);

        $feedback = SelectedAlternative::where('user_id', $request->user()->id)
                                ->where('theme_id', request()->theme_id)
                                ->get();

        foreach($theme->questions as $question){
            array_push($gabarito, $question->right_alternate);
        }

        foreach($feedback as $index => $feed){
            if($feed->alternative == $gabarito[$index]){
                $correct += 1;
            }
        }

        $experience_old = $request->user()->experience;

        $verify_first_play_theme = FullLevel::where('user_id', Auth::user()->id)
                                    ->where('theme_id', request()->theme_id)
                                    ->where('school_id', $school->id)
                                    ->first();

        if($verify_first_play_theme != null){
            $verify_first_play_theme->update([
                'points_acquired' => $correct
            ]);
            $experience = $experience_old;
        }else{
            FullLevel::create([
                'school_id' => $school->id,
                'user_id'   => $request->user()->id,
                'theme_id' => request()->theme_id,
                'points_acquired' => $correct
            ]);
            $experience = ($correct * 50) + $experience_old;
        }

        Auth::user()->update([
            'experience' => $experience
        ]);

        foreach($feedback as $index => $feed){
            $feed->delete();
        }

        return response()->json(['acertos' => $correct, 'scores' => $correct * 50]);
    }
}
