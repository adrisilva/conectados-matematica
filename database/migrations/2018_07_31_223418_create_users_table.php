<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUsersTable.
 */
class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('school_id');
            $table->string('name');
            $table->string('registration')->unique()->nullable($value = true);
            $table->date('birth');
            $table->string('course')->nullable($value = true);
            $table->string('series', 10)->nullable($value = true);
            $table->enum('shift', ['Manhã', 'Tarde', 'Noite', 'Integral'])->default('Integral');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('type', ['Aluno', 'Professor', 'ProfessorAdmin'])->default('Aluno');
            $table->string('matter')->nullable($value = true);
            $table->integer('experience')->default(0);
            $table->rememberToken();
            $table->softDeletes();
			$table->timestamps();
			
			$table->foreign('school_id')
                ->references('id')
                ->on('schools')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}
}
