<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function firstAccess($school)
    {
        return view('auth.password.change', compact('school'));
    }

    public function changePassword(ChangePasswordRequest $request, $school)
    {
        $user = (Auth::check() ? $user = Auth::user() : Auth::guard('admin')->user());

        $user->update([
            'first_access' => 0,
            'password'  => bcrypt($request->password)
        ]);

        if(Auth::check()){
            return redirect('home');
        }

        return redirect('admin/dashboard');
    }
}
