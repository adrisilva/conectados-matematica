<?php

namespace App\Policies;

use App\Entities\User;
use App\Entities\Admin;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function boot()
    {
        //$this->registerPolicies();
    }
}
