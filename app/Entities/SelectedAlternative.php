<?php

namespace App\Entities;

use App\Scopes\SchoolScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SelectedAlternative extends Model
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScope());
    }

    protected $fillable = ['user_id', 'question_id', 'alternative', 'school_id','theme_id'];
}
