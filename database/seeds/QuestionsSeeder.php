<?php

use App\Entities\Question;
use App\Entities\Theme;
use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class QuestionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	for($i = 1; $i <= 4; $i++){
    		$theme = Theme::find($i);

	    	Question::create([
				'school_id'     	=> '1',
	    		'theme_id'			=> $theme->id,
	    		'question'			=> 'Teste_Pergunta',
	    		'alternative_a'		=> 'teste_Resposta',
	    		'alternative_b'		=> 'teste_Resposta',
	    		'alternative_c'		=> 'teste_Resposta',
	    		'alternative_d'		=> 'teste_Resposta',
	    		'alternative_e'		=> 'teste_Resposta',
	    		'right_alternate'	=> 'a',
	    		'resolution'		=> 'resolução',
	    		'score'				=> 5
	    	]); 
    	}
        
    }
}
