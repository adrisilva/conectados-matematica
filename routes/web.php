<?php

Route::group(['domain' => '{school}.'.env('MAIN_DOMAIN')], function(){

	//ROTAS DO SITE
	Route::get('/', 'SiteController@index')->name('site.index');
    Route::post('login', 'Auth\LoginController@login')->name('site.login');
    Route::post('logout', 'Auth\LoginController@logout')->name('site.logout');

    Route::get('admin', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('admin', 'Admin\Auth\LoginController@login');
    Route::post('admin/logout', 'Admin\Auth\LoginController@logout')->name('admin.logout');

    Route::get('first-access', 'UserController@firstAccess')->name('user.first_access');
    Route::put('first-access', 'UserController@changePassword')->name('user.change_password');

    Route::group(['middleware' => ['first_access']], function(){
        //ROTAS DA PLATAFORMA
        Route::get('home', 'PlatformController@index')->name('platform.home');
        Route::get('load-themes', 'PlatformController@loadThemes')->name('platform.load.themes');
        Route::get('aula/tema/{theme_id}', 'PlatformController@showClass')->name('platform.class');
        Route::get('quiz/{theme_id}/iniciar', 'QuizController@start')->name('quiz.start');
        Route::get('question/{id}/load', 'QuizController@loadQuestion')->name('question.get');
        Route::post('question/verify', 'QuizController@verifyQuestion')->name('question.verify');
        Route::get('quiz/{theme_id}/calculate', 'QuizController@calculatePoints')->name('quiz.calculate');
        Route::get('ranking', 'PlatformController@ranking')->name('platform.ranking');

        //ROTAS DO ADMINISTRADOR
        Route::group(['prefix' => 'admin'], function(){
            Route::group(['namespace' => 'Admin'], function(){
                //rotas de login
                Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');

                Route::get('conteudos', 'ContentController@index')->name('content.index');
                Route::get('conteudos/novo', 'ContentController@create')->name('content.create');
                Route::post('conteudos/novo', 'ContentController@store')->name('content.store');
                Route::get('conteudos/{id}/edit', 'ContentController@edit')->name('content.edit');
                Route::put('conteudos/{id}/edit', 'ContentController@update')->name('content.update');
                Route::delete('conteudos/{id}/delete', 'ContentController@delete')->name('content.delete');
            });
        });
    });
});