<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateThemesTable.
 */
class CreateThemesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('themes', function(Blueprint $table) {
			$table->increments('id');
			$table->unsignedInteger('school_id');
            $table->string('title');
            $table->text('description');
            $table->string('link_class');
            $table->softDeletes();
			$table->timestamps();
			
			$table->foreign('school_id')
                ->references('id')
                ->on('schools')
                ->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('themes');
	}
}
