@extends('adminlte::page') 
@section('title', $theme->title) 
@section('content_header')
<h1>
    Quiz: {!! $theme->title !!}
</h1>
@endsection


@section('content')

<style>
    #preloader{position:fixed;top:0; left:0;right:0;bottom:0;background:#ECF0F1;z-index:9999;height:100%;width:100%;}
    #status{width:920px;height:532px;position:absolute;left:50%;top:50%;margin:-266px 0 0 -460px;background:url({{asset('img/preloader.gif')}}) center no-repeat;}
</style>

<div id="preloader">
  <div id="status"></div>
</div>


<div class="box">
    <div class="box-header">
        <h3 class="box-title" id='title_question'></h3>
    </div>
    <div class="box-body">

        <div class="radio">
            <label>
                  <input type="radio" name="alternativa" value="a">
                  <span id="alt_a"></span>
                </label>
        </div>

        <div class="radio">
            <label>
                    <input type="radio" name="alternativa" value="b">
                    <span id="alt_b"></span>
                </label>
        </div>

        <div class="radio">
            <label>
                    <input type="radio" name="alternativa" value="c">
                    <span  id="alt_c"></span>
                </label>
        </div>

        <div class="radio">
            <label>
                    <input type="radio" name="alternativa" value="d">
                    <span  id="alt_d"></span>
                </label>
        </div>

        <div class="radio">
            <label>
                    <input type="radio" name="alternativa" value="e">
                    <span id="alt_e"></span>
                </label>
        </div>

    </div>
    <div class="box-footer">
        <button class="btn btn-success" id="btn_next">Próxima Pergunta</button>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="title_question_modal"></h4>
            </div>
            <div class="modal-body" id="resolution_modal">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btn_continue">Continuar</button>
            </div>
        </div>
    </div>
</div>

{{--MODAL QUE APARECE NA CONCLUSAO DO QUIZ--}}
<div class="modal modal-success fade" id="modal-success"  data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Parabéns</h4>
            </div>
            <div class="modal-body">
                <p>
                    Nível Finalizado!
                </p>
                <p id="acertos">

                </p>
                <p id="points">

                </p>
            </div>
            <div class="modal-footer">
                <a href="{!! url('home') !!}" class="btn btn-outline">Concluir</a>
            </div>
        </div>
    </div>
</div>



@section('js')
<script>
    var questions_ids = @json($questions_id);
    var last_issue_show = '';

    loadQuestion();

    $('#btn_next').on('click', function(){
        var url_verify = '{!! url('question/verify') !!}';
        
        axios.post(url_verify, {
            alternative: $("input[name='alternativa']:checked").val(),
            question_id: last_issue_show,
            theme_id: '{!! request()->theme_id !!}',
            csrf_token: '{!! csrf_token() !!}'
        })
        .then(function(response){
            $('#resolution_modal').html(response.data[0].resolution);
            $('#title_question_modal').text(response.data[0].question);
            $('#myModal').modal('show');
        });
    });

    $('#btn_continue').on('click', function(){
        questions_ids.shift();
        $("input[name='alternativa']").prop('checked', false);
        $('#myModal').modal('hide');
        loadQuestion();
    });

    function loadQuestion(){

        if(questions_ids.length == 0){
            calculate();
            $('#modal-success').modal('show');
        }else{
            var url = '{!! url('question/{id}/load') !!}'.replace('{id}', questions_ids[0]);
        
            axios.get(url)
            .then(function (response) {
                $('#title_question').text(response.data[0].question);

                $('#alt_a').text(response.data[0].alternative_a);
                $('#alt_b').text(response.data[0].alternative_b);
                $('#alt_c').text(response.data[0].alternative_c);
                $('#alt_d').text(response.data[0].alternative_d);
                $('#alt_e').text(response.data[0].alternative_e);

                last_issue_show = response.data[0].id;
            });
        }

    }

    function calculate(){
        var url = '{!! url('quiz/'.request()->theme_id.'/calculate') !!}';
        axios.get(url)
            .then(function (response) {
                $('#acertos').text("Total de acertos: " + response.data.acertos);
                $('#points').text("Pontuação adquirida: " + response.data.scores);
            });
    }

    $('#preloader').delay(1000).fadeOut("slow");

</script>
@endsection
 
@stop