@extends('adminlte::page')

@section('title', 'Home')

@section('content_header')
    <h1>
    	{{ session('success') }}
    </h1>
@stop

@section('content')
	@include('plataforma.themes')
@stop