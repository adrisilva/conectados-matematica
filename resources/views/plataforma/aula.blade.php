@extends('adminlte::page') 
@section('title', $theme->title) 
@section('content_header')
<h1>
    {!! $theme->title !!}
</h1>
@endsection
 
@section('content')
<div class="row">
    <div class="col-md-12">
        <iframe width="880" height="450" src="{!! $theme->link_class !!}"></iframe>
    </div>
</div>
<div class="row">

    <div class="col-sm-12">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button class="btn btn-primary" type="button" onclick="history.go(-1)"><i class="fa fa-backward"></i> Voltar</button>
            <a class="btn btn-success" href="{!! url('quiz/'.$theme->id.'/iniciar') !!}"><i class="fa fa-play-circle"></i> Iniciar Quiz</a>
        </div>
    </div>

</div>

@stop