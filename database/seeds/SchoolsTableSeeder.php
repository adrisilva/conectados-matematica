<?php

use App\Entities\School;
use Illuminate\Database\Seeder;

class SchoolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::create([
            'name'  => 'Onelio',
            'slug' => 'onelio'
        ]);
    }
}
