@extends('adminlte::page')

@section('content_header')
    <h1>Novo Conteúdo</h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Conteúdos</a></li>
        <li><a href="">Novo</a></li>
    </ol>
@stop

@section('content')
    <form action="{!! url('admin/conteudos/novo') !!}" method="POST">
        @csrf
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="form-group">
                        <label for="" class="col-sm-2">Título</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="title">
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group">
                        <label for="" class="col-sm-2">Descrição</label>
                        <div class="col-sm-10">
                            <textarea name="description" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group">
                        <label for="" class="col-sm-2">Link da Vídeo aula</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="link_class">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <h3>Adicionar Questões</h3>
        <br>
        @for($i = 0; $i <= 4; $i++)

            <div class="box">
                <div class="box-header" data-widget="collapse" data-toggle="tooltip">
                    <h4>Questão {!! $i + 1 !!}</h4>
                    <div class="pull-right box-tools">
                        <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <label for="" class="col-sm-2">Enunciado</label>
                        <div class="col-sm-10">
                            <textarea name="questions[{!! $i !!}][question]" class="form-control"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="" class="col-sm-2">Alternativa A</label>
                        <div class="col-sm-10">
                            <input type="text" name="questions[{!! $i !!}][alternative_a]" class="form-control" value="">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="" class="col-sm-2">Alternativa B</label>
                        <div class="col-sm-10">
                            <input type="text" name="questions[{!! $i !!}][alternative_b]" class="form-control" value="">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="" class="col-sm-2">Alternativa C</label>
                        <div class="col-sm-10">
                            <input type="text" name="questions[{!! $i !!}][alternative_c]" class="form-control" value="">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="" class="col-sm-2">Alternativa D</label>
                        <div class="col-sm-10">
                            <input type="text" name="questions[{!! $i !!}][alternative_d]" class="form-control" value="">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="" class="col-sm-2">Alternativa E</label>
                        <div class="col-sm-10">
                            <input type="text" name="questions[{!! $i !!}][alternative_e]" class="form-control" value="">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label for="" class="col-sm-2">Resolução</label>
                        <div class="col-sm-10">
                        <textarea id="summernote" name="questions[{!! $i !!}][resolution]" class="form-control summernote">
                            
                        </textarea>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <label for="" class="col-sm-2">Alternativa Correta</label>
                        <div class="col-sm-10">
                            <select name="questions[{!! $i !!}][right_alternate]" class="form-control">
                                <option value="a">A</option>
                                <option value="b">B</option>
                                <option value="c">C</option>
                                <option value="d">D</option>
                                <option value="e">E</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        @endfor

        <button type="submit" class="btn btn-success btn-block">Salvar</button>
    </form>
@section('js')
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                 toolbar: [
            [ 'style', [ 'style' ] ],
            [ 'font', [ 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear'] ],
            
            [ 'fontsize', [ 'fontsize' ] ],
            [ 'color', [ 'color' ] ],
            [ 'para', [ 'ol', 'ul', 'paragraph', 'height' ] ],
            [ 'view', [ 'undo', 'redo', 'fullscreen', 'codeview', 'help' ] ]
        ]
            });
        });
    </script>
@stop
@stop
