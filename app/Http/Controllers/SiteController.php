<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
	public function index($school)
	{
		return view('site.index', compact('school'));
	}

}
