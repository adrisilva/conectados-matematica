<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class FirstAccessVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = (Auth::check() ? $user = Auth::user() : Auth::guard('admin')->user());

        if($user->first_access){
            return redirect('first-access');
        }
        return $next($request);
    }
}
