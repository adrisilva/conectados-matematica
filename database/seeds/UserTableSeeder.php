<?php

use App\Entities\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'school_id'     => '1',
        	'name' 			=> 'José Silva',
        	'registration'	=> '123456',
        	'birth'			=> '2000/05/07',
        	'course'		=> 'Informática',
        	'series'        => '3',
        	'shift'         => 'Integral',
        	'type'          => 'Aluno',
        	'email'         => 'jose@gmail.com',
        	'password'      => bcrypt(123456),
        ]);

        User::create([
            'school_id'     => '1',
            'name'          => 'Paula Silva',
            'registration'  => '654321',
            'birth'         => '1985/06/07',
            'type'          => 'Professor',
            'matter'        => 'Física',
            'email'         => 'paula@gmail.com',
            'password'      => bcrypt(123456),
        ]);

        User::create([
            'school_id'     => '1',
            'name'          => 'Ricardo Silva',
            'registration'  => '852147',
            'birth'         => '1985/09/10',
            'type'          => 'ProfessorAdmin',
            'matter'        => 'Matemática',
            'email'         => 'ric@gmail.com',
            'password'      => bcrypt(123456),
        ]);
    }
}
