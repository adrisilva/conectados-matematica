<?php

namespace App\Observers;

use App\Entities\Question;
use App\Entities\School;

class QuestionObserver
{
    private $school;

    public function __construct()
    {
        $this->school = School::where('slug', request()->school)->first();
    }
    /**
     * Handle the question "created" event.
     *
     * @param  \App\Entities\Question  $question
     * @return void
     */
    public function created(Question $question)
    {
        //
    }

    /**
     * Handle the question "updated" event.
     *
     * @param  \App\Entities\Question  $question
     * @return void
     */
    public function updated(Question $question)
    {
        //
    }

    /**
     * Handle the question "deleted" event.
     *
     * @param  \App\Entities\Question  $question
     * @return void
     */
    public function deleted(Question $question)
    {
        //
    }

    /**
     * Handle the question "restored" event.
     *
     * @param  \App\Entities\Question  $question
     * @return void
     */
    public function restored(Question $question)
    {
        //
    }

    /**
     * Handle the question "force deleted" event.
     *
     * @param  \App\Entities\Question  $question
     * @return void
     */
    public function forceDeleted(Question $question)
    {
        //
    }

    public function creating(Question $question)
    {
        $question->school_id = $this->school->id;
    }
}
