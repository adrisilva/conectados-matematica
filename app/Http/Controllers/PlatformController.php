<?php

namespace App\Http\Controllers;

use App\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Entities\Theme;

class PlatformController extends Controller
{
	public function __construct()
	{
        if(auth()->guard('admin')->check()){
            return redirect()->back();
        }
		$this->middleware('auth');
	}

    public function index($school)
    {
    	return view('plataforma.index', compact('school'));
    }

    public function loadThemes()
    {
        $themes = Theme::with(['scores' => function($query){
            $query->where('user_id', auth()->user()->id);
        }])->get();

        return response()->json($themes, 200);
    }

    public function showClass($school, $theme_id)
    {
        $theme = Theme::find($theme_id);

        return view('plataforma.aula', compact('theme'));
    }

    public function ranking()
    {
        $users = User::select('name', 'experience')
            ->where('id', '!=', Auth::user()->id)
            ->orderBy('experience', 'DESC')
            ->paginate(5);

        return view('plataforma.ranking', compact('users'));
    }
}
