<?php

namespace App\Entities;

use App\Scopes\SchoolScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Theme.
 *
 * @package namespace App\Entities;
 */
class Theme extends Model implements Transformable
{
	use SoftDeletes;
    use TransformableTrait;

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new SchoolScope);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'link_class', 'school_id'];

    protected $dates = ['deleted_at'];

    public static $rules = [
        'title'         => 'required',
        'description'   => 'required',
        'link_class'    => 'required',
        'questions.*.question'   => 'required',
        'questions.*.alternative_a'   => 'required',
        'questions.*.alternative_b'   => 'required',
        'questions.*.alternative_c'   => 'required',
        'questions.*.alternative_d'   => 'required',
        'questions.*.alternative_e'   => 'required',
        'questions.*.resolution'   => 'required',
        'questions.*.right_alternate'   => 'required',
    ];

    public function scores()
    {
    	return $this->hasMany(FullLevel::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}
