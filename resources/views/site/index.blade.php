<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Conectados Pela Matemátoca</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="robots" content="all,follow">
  <!-- Bootstrap CSS-->
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <!-- Font Awesome & Pixeden Icon Stroke icon font-->
  <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/pe-icon-7-stroke.css')}}">
  <!-- Google fonts - Roboto Condensed & Roboto-->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700|Roboto:300,400">
  <!-- lightbox-->
  <link rel="stylesheet" href="{{asset('css/lightbox.min.css')}}">
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="{{asset('css/style.default.css')}}" id="theme-stylesheet'>
  <!-- Custom stylesheet - for your changes-->
  <link rel="stylesheet" href="{{asset('css/custom.css')}}">
  <!-- Favicon-->
  <link rel="shortcut icon" href="favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  <link rel="shortcut icon" href={{asset("favicon.png")}}>
  </head>
      <body class="home">
        <!-- navbar-->
        <header class="header">
          <div role="navigation" class="navbar navbar-default">
            <div class="container">
              <div class="navbar-header"><a href="{!! url('/') !!}" class="navbar-brand"><img src="{{asset('img/conectados-mt.png')}}"></a>
                <div class="navbar-buttons">
                  <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu<i class="fa fa-align-justify"></i></button>
                </div>
              </div>
              <div id="navigation" class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                  <li><a href="http://{!! request()->school.'.'.env('MAIN_DOMAIN') !!}"><i class="fa fa-arrow-circle-left"></i>Voltar</a></li>
                  <li><a href="https://alimentodamente.com.br/adm/site_adm/public/contato">Contato</a></li>
                  @if(Auth::guard('admin')->check())
                  <li><a href="{{ route('dashboard.index', $school) }}">Painel Administrativo</a></li>
                  @elseif(Auth::check())
                  <li><a href="{{ route('platform.home', $school) }}">Plataforma</a></li>
                  @else
                  {{-- <a href="{{ route('login') }}">Login</a> --}}
                  <a href="#" data-toggle="modal" data-target="#login-modal" class="btn navbar-btn btn-white pull-left"><i class="fa fa-sign-in"></i>Login</a>
                  @endauth
                </ul>
              </div>
            </div>
          </div>
        </header>
        <!-- JANELA MODAL DE LOGIN-->
        <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true" class="modal fade">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
                <h4 id="Login" class="modal-title">Login</h4>
              </div>
              <div class="modal-body">
                <form action="{{ url('login') }}" method="post">
                  @csrf

                  <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                    placeholder="E-Mail">
                    {{-- <span class="glyphicon glyphicon-envelope form-control-feedback"></span> --}}
                    @if ($errors->has('email'))
                    <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                    placeholder="Senha">
                    {{-- <span class="glyphicon glyphicon-floppy-disk form-control-feedback"></span> --}}
                    @if ($errors->has('password'))
                    <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                  </div>

                  <p class="text-center">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                  </p>
                </form>
                <p class="text-center text-muted">Não é cadastrado?</p>
                <p class="text-center text-muted"><font color="#f06190"><strong>Procure a biblioteca da escola!</strong></font></a> É facil, rápido, gratuito e sempre será!</p>

              </div>
            </div>
          </div>
        </div>
        <!-- *** LOGIN MODAL END ***-->
        <div id="carousel-home" data-ride="carousel" class="carousel slide carousel-fullscreen carousel-fade">
          <!-- Indicators-->
          <ol class="carousel-indicators">
            <li data-target="#carousel-home" data-slide-to="0" class="active"></li>

          </ol>
          <!-- Wrapper for slides-->
          <div role="listbox" class="carousel-inner">
            <div style="background-image: url('img/carousel1.jpg');" class="item active">

            </div>

          </div>
        </div>
        <section class="background-gray-lightest negative-margin">
          <div class="container">
            <h1> O que é o Conectados pela Matemática?</h1>
            <p class="lead">O Conectados MT (Conectados pela matemática) é um projeto que visa o aprendizado em matemática e a prática de exercícios de forma bem simples. Buscamos através do nosso projeto, incentivar a resolução de exercícios matemáticos fora do contexto de sala de aula, para que se torne um hábito como ler um livro por exemplo. O projeto Conectados MT vai além do ensino, também envolve o quesito de vencer barreiras como: medo, preconceito e o receio que as pessoas têm da matemática. Através de uma plataforma bem interativa e simples, desejamos o melhor aprendizado a todos os usuários.</p>

          </div>
        </section>
        <!--   *** SERVICES ***-->
        <section class="background-gray-lightest">
          <div class="container clearfix">
            <div class="row services">
              <div class="col-md-12">
                <h2>Serviços</h2>
                <p class="lead margin-bottom--medium"> O que o Conectados pela matemática pode oferecer para você?.</p>
                <div class="row">
                  <div class="col-sm-4">
                    <div class="box box-services">
                      <div class="icon"><i class="pe-7s-film"></i></div>
                      <h4 class="services-heading">Vídeo aulas</h4>
                      <p>Veja sempre uma vídeo aula sobre o tema antes de iniciar as questões.</p>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="box box-services">
                      <div class="icon"><i class="pe-7s-note2"></i></div>
                      <h4 class="services-heading">QUESTÕES</h4>
                      <p>Resolva exercícios sobre os assuntos em que você tem interesse. Nada melhor do que praticar, não é mesmo?</p>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="box box-services">
                      <div class="icon"><i class="pe-7s-like2"></i></div>
                      <h4 class="services-heading">RESOLUÇÕES</h4>
                      <p>Todas as nossas questões tem a resolução com uma explicação perfeita para o seu entendimento. </p>
                    </div>
                  </div>
                </div>
              </section>
              <!--   *** SERVICES END ***-->
              <footer class="footer">
                <div class="footer__copyright">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-6">
                        <p>&copy;2017 Your name goes here</p>
                      </div>
                      <div class="col-md-6">
                        <p class="credit">Template by <a href="https://bootstrapious.com/free-templates" class="external">Bootstrapious templates</a></p>
                        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
                      </div>
                    </div>
                  </div>
                </div>
              </footer>
              <!-- Javascript files-->
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
              <script src="{{asset('js/bootstrap.min.js')}}"></script>
              <script src="{{asset('js/jquery.cookie.js')}}"> </script>
              <script src="{{asset('js/lightbox.min.js')}}"></script>
              <script src="{{asset('js/front.js')}}"></script>

            </body>
            </html>
