
<?php

use App\Entities\Admin;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'school_id' => '1',
        	'name'  	=> 'admin',
        	'email' 	=> 'admin@gmail.com',
            'phone'     => '40028922',
        	'password' 	=> bcrypt('admin'),
            'active'    => true,
        ]);
    }
}
