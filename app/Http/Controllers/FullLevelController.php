<?php

namespace App\Http\Controllers;

use App\Entities\FullLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FullLevelController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
}
