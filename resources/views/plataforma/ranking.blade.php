@extends('adminlte::page')
@section('title', 'Ranking')
@section('content_header')
    <h1>
        Ranking Escolar
    </h1>
@endsection

@section('content')
   <div class="box">
       <div class="box-header">
           <strong>Sua Pontuação: {!! Auth::user()->experience !!}</strong>
       </div>
       <div class="box-body">
           <div class="table-responsive">

               <table class="table" id="datatables">
                   <thead>
                   <tr>
                       <th>Nome</th>
                       <th>Pontuação</th>
                   </tr>
                   </thead>

                   <tbody>
                       @forelse($users as $user)
                           <tr>
                               <th>{!! $user->name !!}</th>
                               <th>{!! $user->experience !!}</th>
                           </tr>
                       @empty
                            <tr>
                                <th colspan="2">Nenhum Usuário Encontrado</th>
                            </tr>
                       @endforelse
                   </tbody>


               </table>
               {!! $users->links() !!}
           </div>
       </div>
   </div>
@stop

