<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
//    use SoftDeletes;
    
    protected $fillable = ['slug', 'name'];
}
